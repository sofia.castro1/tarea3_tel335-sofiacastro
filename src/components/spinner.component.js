import { Spinner } from 'react-bootstrap'

export default function SpinnerLoader (props) {
    if (props.dataLoaded) {
        return null
    } else {
        return (
            <div style={{position:'fixed', top:'50%', left:'50%'}}>
                <Spinner animation="border" variant="primary" />
            </div>
        )
    }
}